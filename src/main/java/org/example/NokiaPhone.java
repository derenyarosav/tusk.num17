package org.example;

public class NokiaPhone extends Phone implements PhoneConnection, PhoneMedia {


    public NokiaPhone(int memory, String model, int power, int year) {
        super(memory, model, power, year);
    }

    @Override
    public void call() {
        System.out.println("Calling...");
    }

    @Override
    public void takecall() {
        System.out.println("take the call...");
    }

    @Override
    public void rejecktcall() {
        System.out.println("to reject call...");
    }

    @Override
    public void vibration() {
        System.out.println("Vibration mode is on");
    }

    @Override
    public void SilentMode() {
        System.out.println("Silent mode is mode");
    }

    @Override
    public void DontDistrub() {
        System.out.println("No disturbance mode");
    }

    @Override
    public void photo() {
        System.out.println("make a photo");
    }

    @Override
    public void filming() {
        System.out.println("start to filming..");
    }

    @Override
    public void TurnOnTheRecorder() {
        System.out.println("Turning on the recorder..");
    }

    @Override
    public void NightSceneMode() {
        System.out.println("Night scene mode is on");
    }

    @Override
    public void ScreenRecording() {
        System.out.println("Screen recording");
    }


}
